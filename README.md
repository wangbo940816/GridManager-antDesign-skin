<h1 align="center">Welcome to gridmanager-antdesign-skin 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.0.0-blue.svg?cacheSeconds=2592000" />
  <a href="https://github.com/BoWang816/GridManager-antDesign-skin#readme" target="_blank">
    <img alt="Documentation" src="https://img.shields.io/badge/documentation-yes-brightgreen.svg" />
  </a>
  <a href="https://github.com/BoWang816/GridManager-antDesign-skin/graphs/commit-activity" target="_blank">
    <img alt="Maintenance" src="https://img.shields.io/badge/Maintained%3F-yes-green.svg" />
  </a>
  <a href="https://github.com/BoWang816/GridManager-antDesign-skin/blob/master/LICENSE" target="_blank">
    <img alt="License: ISC" src="https://img.shields.io/github/license/BoWang816/gridmanager-antdesign-skin" />
  </a>
</p>

> GridManager适配ant Design组件库样式

### 🏠 [Homepage](https://github.com/BoWang816/GridManager-antDesign-skin#readme)

## Rely

[gridmanager-react](https://www.npmjs.com/package/gridmanager-react)

## Install

```sh
npm install gridmanager-antdesign-skin
```

## Usage

```sh
<script src="../node_modules/gridmanager-antdesign-skin"></script>
```

## Author

👤 **bo.wang**

* Website: https://bowang816.github.io
* Github: [@BoWang816](https://github.com/BoWang816)

## Show your support

Give a ⭐️ if this project helped you!

## 📝 CopyRight

Copyright © 2020 [bo.wang](https://github.com/BoWang816).<br />

***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_